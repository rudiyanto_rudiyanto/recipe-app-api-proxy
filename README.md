# recipe app api proxy

NGINX proxy app for our recipe app API

## Usage


### Environtment Variables

 * `LISTEN_PORT` - port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward request to (default: `app`)
 * `APP_PORT` - Port of the app to forward request to (default: `9000`)

